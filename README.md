App intended to work as a live chat room for watching youtube.

To see more commit history go to:
https://bitbucket.org/tryceattack/youtube

What is currently working.

Youtube functionality: http://pure-eyrie-1654.herokuapp.com/search

Real time chat room:
http://pure-eyrie-1654.herokuapp.com/channels/1
Go on a incognito and a regular browser, and observe the real-time response.

Other features: Once you pull the code, deploy it on a client-sided server, submit a message on the client-sided server website, you'll also see an update on the heroku-hosted server website!

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact