class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  require 'rubygems'

require 'google/api_client'

 
  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end


   def create
    @user = User.new(user_params)
     Pusher['survey-channel'].trigger('data-changed', [@user.name,@user.email])
    if @user.save
      respond_to do |format|
        format.html {redirect_to @user}
        format.js
      end
      else
        render 'new'
      end
  end
  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])
    @users= User.all
  end


  def live_button


  end

def search
    
    
    opts={}
    client, youtube = get_service
opts[:q] ='kings of cali mew2king'
opts[:max_results]=26
    begin
      # Call the search.list method to retrieve results matching the specified
      # query term.
      if params[:search]
        opts[:q] = "#{params[:search]}"
      end
          
      if params[:max_results]
        opts[:max_results]="#{params[:max_results]}"
      end

      @search_response = client.execute!(
        :api_method => youtube.search.list,
        :parameters => {
          :part => 'snippet',
          :q => opts[:q],
          :maxResults => opts[:max_results]
        }
      )

      @videos = []
      @channels = []
      @playlists = []
      @likes =[]
      # Add each result to the appropriate list, and then display the lists of
      # matching videos, channels, and playlists.
      @search_response.data.items.each do |search_result|
        case search_result.id.kind
          when 'youtube#video'
            @videos << "#{search_result.id.videoId}"
           
          when 'youtube#channel'
            @channels << "#{search_result.snippet.title} (#{search_result.id.channelId})"
          when 'youtube#playlist'
           @playlists << "#{search_result.snippet.title} (#{search_result.id.playlistId})"
        end
      end

      @like_response = client.execute!(
        :api_method => youtube.videos.list,
        :parameters => {
          :part => 'statistics',
          :id => "#{@videos.join(",")}"
        }
      )

      @like_response.data.items.each do |like_result|
            @likes << "#{like_result.statistics.likeCount}"
      end

     # puts "Videos:\n", videos, "\n"
      #puts "Channels:\n", channels, "\n"
      #puts "Playlists:\n", playlists, "\n"
    #rescue Google::APIClient::TransmissionError => e
     # puts e.result.body
    #end
    

    end
  end
  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
 

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :email,:password,:password_confirmation)
    end


    def get_service
  client = Google::APIClient.new(
    :key => DEVELOPER_KEY,
    :authorization => nil,
    :application_name => $PROGRAM_NAME,
    :application_version => '1.0.0'
  )
  youtube = client.discovered_api(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION)

  return client, youtube
end

DEVELOPER_KEY = 'AIzaSyCZgl9jnag2Nub8C7mj0rZWS332DyVey7o'
YOUTUBE_API_SERVICE_NAME = 'youtube'
YOUTUBE_API_VERSION = 'v3'
end